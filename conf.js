exports.config = {

    seleniumAddress: 'http://localhost:4444/wd/hub',

    capabilities: {
        'browserName': 'phantomjs',
        'phantomjs.cli.args': ['--ignore-ssl-errors=true']
    },

    jasmineNodeOpts: {
        showColors: true
    },

    onPrepare: function() {
        jasmine.getEnv()
            .addReporter(require('./spec/helpers/points-reporter').reporter);
    }

};
