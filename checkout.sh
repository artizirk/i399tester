#!/bin/bash

set -e

USER=$1
TAG=$2
TMP="./.tmp"
SUB_DIR=""

if [ $# -eq 0 ];then
  echo "Usage: $0 <user_name> <tag>"
  exit 1
fi

if [ ${TAG} == "hw2" ] || [ ${TAG} == "hw3" ]; then
  SUB_DIR="ng1"
fi

if [ ${TAG} == "hw4" ]; then
  SUB_DIR="ng2"
fi

if [ -z "${SUB_DIR}" ]; then
  echo 'unknown tag'
  exit 1
fi

git clone https://bitbucket.org/${USER}/i399 $TMP

(cd "${TMP}" && \
 git checkout ${TAG} && \
 cd "${SUB_DIR}" && \
 npm install && \
 if [ ${TAG} == "hw4" ]; then npm run build; fi && \
 npm start)
