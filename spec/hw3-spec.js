'use strict';

var BASE_URL = 'http://localhost:3000/';

describe('Application part 3', function() {

    it = extendIt(it); fit = extendIt(fit); xit = extendIt(xit);

    it('should insert contact with name and phone number', done => {
        var sampleData = getSampleData();
        var data = { name : sampleData.name, phone : sampleData.phone };

        post('api/contacts', data).then(() => {
            return get('api/contacts');
        }).then(contacts => {
            expect(contacts.map(each => each.name)).toContain(sampleData.name);
            expect(contacts.map(each => each.phone)).toContain(sampleData.phone);
            done();
        });

    }).deductedOnFailure(10);


    it('should modify contact', done => {
        var name = getSampleData().name;
        var nameChanged = name + 1;

        post('api/contacts', { name : name }).then(() => {
            return get('api/contacts');
        }).then(contacts => {
            var fetchedItem = contacts.filter(each => each.name === name).pop();
            fetchedItem.name = nameChanged;
            return put('api/contacts/' + fetchedItem._id, fetchedItem);
        }).then(() => {
            return get('api/contacts');
        }).then(contacts => {
            expect(contacts.map(each => each.name)).not.toContain(name);
            expect(contacts.map(each => each.name)).toContain(nameChanged);
            done();
        });

    }).deductedOnFailure(10);


    it('should delete contact', done => {
        var name = getSampleData().name;

        post('api/contacts', { name : name }).then(() => {
            return get('api/contacts');
        }).then(contacts => {
            var id = contacts.filter(each => each.name === name).pop()._id;
            return del('api/contacts/' + id);
        }).then(() => {
            return get('api/contacts');
        }).then(contacts => {
            expect(contacts.map(each => each.name)).not.toContain(name);
            done();
        });

    }).deductedOnFailure(5);


    it('should delete multiple contacts', done => {
        var name = 'Jack';

        post('api/contacts', { name : name }).then(() => {
            return post('api/contacts', { name : name });
        }).then(() => {
            return get('api/contacts');
        }).then(contacts => {
            var ids = contacts
                .filter(each => each.name === name)
                .map(each => each._id);
            return post('api/contacts/delete', ids);
        }).then(() => {
            return get('api/contacts');
        }).then(contacts => {
            expect(contacts.map(each => each.name)).not.toContain(name);
            done();
        });

    }).deductedOnFailure(2);
});

var request = require('request');

function get(url) {
    return new Promise((resolve, reject) => {
        request(getUrl(url), function(error, response, body) {
            if (error) {
                reject(error);
            } else if (response.statusCode !== 200) {
                reject(body);
            } else {
                resolve(JSON.parse(body));
            }
        });
    });
}

function del(url) {
    return modifyCommon(url, 'DELETE', undefined);
}

function post(url, data) {
    return modifyCommon(url, 'POST', data);
}

function put(url, data) {
    return modifyCommon(url, 'PUT', data);
}

function modifyCommon(url, method, data) {
    return new Promise((resolve, reject) => {
        request({
            headers: { 'Content-type': 'application/json' },
            url : getUrl(url),
            method: method,
            body: JSON.stringify(data)
        }, (error, response, body) => {
            if (error) {
                reject(error);
            } else if (response.statusCode < 200 || response.statusCode >= 300) {
                reject(body);
            } else {
                resolve();
            }
        });
    });
}

function getSampleData() {
    var time = new Date().getTime();
    return {
        name : time,
        phone : time + 1
    };
}

function getUrl(path) {
    return BASE_URL.replace(/\/$/, '') + '/' + path;
}

function extendIt(it) {
    return require('./helpers/points-reporter').extendIt(it);
}
