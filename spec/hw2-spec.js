'use strict';

var BASE_URL = 'http://localhost:3000/';

describe('Application part 2', function () {

    it = extendIt(it); fit = extendIt(fit); xit = extendIt(xit);

    beforeEach(function() {
        goTo(BASE_URL);
    });

    it('menu links should change url', function () {
        expect(currentUrl()).toBe(getUrl('#/search'));

        link('menu-new').click();

        expect(currentUrl()).toBe(getUrl('#/new'));

        link('menu-search').click();

        expect(currentUrl()).toBe(getUrl('#/search'));

    }).deductedOnFailure(10);


    it('should insert contact with name and phone number', function () {

        link('menu-new').click();

        var sampleData = getSampleData();

        input('name-input').setValue(sampleData.name);
        input('phone-input').setValue(sampleData.phone);

        link('save-link').click();

        expect(currentUrl()).toBe(getUrl('#/search'));

        expect(element(by.tagName('table')).getText()).toContain(sampleData.name);
        expect(element(by.tagName('table')).getText()).toContain(sampleData.phone);

    }).deductedOnFailure(10);


    it('selecting a contact should change its row\'s css class', function () {
        var name = getSampleData().name;
        insertContact(name);

        rowContainingText(name).checkBox().click();

        var cssClassesList = rowContainingText(name).cssClasses();

        expect(cssClassesList).toContain('selected');

    }).deductedOnFailure(2);


    it('should be able to edit contacts', function () {
        var sampleData = getSampleData();
        var changedName = sampleData.name + 1;
        insertContact(sampleData.name);

        rowContainingText(sampleData.name).elementWithCssClass('edit-link').click();

        input('name-input').setValue(changedName);
        link('save-link').click();

        expect(currentUrl()).toBe(getUrl('#/search'));
        expect(element(by.tagName('table')).getText()).toContain(changedName);
        expect(element(by.tagName('table')).getText()).not.toContain(sampleData.name);

    }).deductedOnFailure(5);


    it('should ask confirmation on deleting', function () {
        var name = getSampleData().name;
        insertContact(name);

        rowContainingText(name).elementWithCssClass('delete-link').click();

        expect(element(by.id('simple-modal')).isPresent()).toBe(true, "can't find modal");

    }).deductedOnFailure(1);


    it('should delete contacts', function () {

        var name = getSampleData().name;
        insertContact(name);

        expect(element(by.tagName('table')).getText()).toContain(name);

        rowContainingText(name).elementWithCssClass('delete-link').click();

        link('simple-modal-ok').click();

        expect(currentUrl()).toBe(getUrl('#/search'));

        expect(element(by.tagName('table')).getText()).not.toContain(name);

    }).deductedOnFailure(1);


    it('should filter by name and phone', function () {

        insertContact('Jaana');
        insertContact('Jaak');
        insertContact('Tiit', 'aa');
        insertContact('Tiina');

        input('search-string').setValue('aa');

        expect(element(by.tagName('table')).getText()).toContain('Jaana');
        expect(element(by.tagName('table')).getText()).toContain('Jaak');
        expect(element(by.tagName('table')).getText()).toContain('Tiit');
        expect(element(by.tagName('table')).getText()).not.toContain('Tiina');

    }).deductedOnFailure(2);

});

function rowContainingText(searchString) {
    var rows = element.all(by.tagName('tr'))
        .filter(function (each) {
            return each.getText().then(function (text) {
                return text.indexOf(searchString) >= 0;
            })
        });

    expect(rows.count()).toBe(1, 'should find exactly one matching row');

    return {
        elementWithCssClass: elementWithCssClass,
        checkBox: checkBox,
        cssClasses: cssClasses
    };

    function elementWithCssClass(cssClass) {
        var elements = rows.get(0).all(by.css('.' + cssClass));

        expect(elements.count()).toBe(1, 'should find exactly one matching element');

        return elements.get(0);
    }

    function cssClasses() {
        return rows.first().getAttribute('class').then(function (string) {
            return string.split(' ');
        });
    }

    function checkBox() {
        var checkboxes = rows.get(0).all(by.tagName('input'));

        expect(checkboxes.count()).toBe(1, 'should find exactly one checkbox');

        return checkboxes.get(0);
    }
}

function link(id) {
    return element(by.id(id));
}

function goTo(addr) {
    browser.get(addr).then(function () {
        browser.manage().window().setSize(1920, 1080);
    });
}

function getUrl(path) {
    return BASE_URL.replace(/\/$/, '') + '/' + path;
}

function currentUrl() {
    return browser.getCurrentUrl();
}

function getSampleData() {
    var time = new Date().getTime();
    return {
        name : time,
        phone : time + 1
    };
}

function input(id) {
    var input = element(by.id(id));

    return {
        getValue: getValue,
        setValue: setValue,
        isPresent : isPresent
    };

    function isPresent() {
        return input.isPresent();
    }

    function setValue(value) {
        if (value == null || value == undefined) {
            return;
        }

        input.clear().sendKeys(value);
    }

    function getValue() {
        return input.getAttribute('value');
    }
}

function insertContact(name, phone) {
    link('menu-new').click();
    input('name-input').setValue(name);
    input('phone-input').setValue(phone);
    link('save-link').click();
}

function extendIt(it) {
    return require('./helpers/points-reporter').extendIt(it);
}
