echo off

set USER=%1
set TAG=%2
set TMP=./.tmp
set SUB_DIR=

if "%1" == "" (
  echo Usage: %0 user_name tag
  exit /b 1
)

if "%TAG%" == "hw2" (
  set SUB_DIR=ng1
)

if "%TAG%" == "hw3" (
  set SUB_DIR=ng1
)

if "%TAG%" == "hw4" (
  set SUB_DIR=ng2
)

if "%SUB_DIR%" == "" (
  echo unknown tag
  exit /b 1
)

git clone https://bitbucket.org/%USER%/i399 %TMP%

if %errorlevel% neq 0 exit /b 1

cd %TMP%

git checkout %TAG%

if %errorlevel% neq 0 exit /b 1

cd %SUB_DIR%

call npm install

if "%TAG%" == "hw4" (
  call npm run build
)

call npm start
