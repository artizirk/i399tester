var conf = require('./conf.js');

conf.config.useAllAngular2AppRoots = true;
conf.config.allScriptsTimeout = 15000;

exports.config = conf.config;
